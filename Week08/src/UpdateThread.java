
/**
 * This class uses the extends Thread with the run method
 *
 */
public class UpdateThread extends Thread {
    private Counter counter;

    public UpdateThread(Counter counter){
        this.counter = counter;
    }

    public void run(){
        try{
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
        counter.increment();
    }
}

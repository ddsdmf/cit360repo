import java.util.concurrent.atomic.AtomicInteger;

/**
 * This class uses an atomicinterger to count the number of threads run
 */
public class Counter {
    private AtomicInteger value = new AtomicInteger();

    /**
     * adds 1 to the AtomicInteger each time it is called
     */
    public void increment() {
        value.incrementAndGet();
    }

    /**
     * decrement 1 from the AtomicInteger each time it is called
     */
    public void decrement() {
        value.decrementAndGet();
    }

    /**
     * used to get the current value of the AtomicInteger
     * @return AtomicInteger value
     */
    public int get() {
        return value.get();
    }
}

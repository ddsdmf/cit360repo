import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * This is used to run the classes with runnable and thread
 */
public class ThreadsTest {
    static final int NUMBER_THREADS = 100;

    public static void main(String[] args) throws InterruptedException {


        //This is using the extends Thread class
        Counter counterTh = new Counter();

        System.out.println("This is using the class with extends Thread");
        System.out.println("Initial Counter = " + counterTh.get());
        UpdateThread[] threads = new UpdateThread[NUMBER_THREADS];

        // loop that runs the UpdateThread class
        for (int i = 0; i < NUMBER_THREADS; i++){
            threads[i] = new UpdateThread(counterTh);
            threads[i].start();
        }//end of for loop

        for (int i = 0; i < NUMBER_THREADS; i++){
            threads[i].join();
        }//End of for loop

        System.out.println("Final counter = " + counterTh.get());
        System.out.println("=== Expected final counter " + NUMBER_THREADS + " ===");
        System.out.println();

        //This is using the implements Runnable class
        Counter counterRun = new Counter();

        System.out.println("This is using the class with implements Runnable");
        System.out.println("Initial Counter = " + counterRun.get());

        ExecutorService exeService = Executors.newFixedThreadPool(3);

        for (int runs = 0; runs < NUMBER_THREADS; runs++){
            UpdateRunnable run1 = new UpdateRunnable(counterRun);
            exeService.execute(run1);
        }//end of for loop

        //shutting down the executor
        exeService.shutdown();
        //Wait for the executor to shutdown
        while (!exeService.isTerminated()){
            //This loop is waiting for the ExecutorService to complete all the thread
            //before the program continues to run
        }//end of while loop

        System.out.println("Final counter = " + counterRun.get());
        System.out.println("=== Expected final counter " + NUMBER_THREADS + " ===");


    }//End of Main
}//End of Class

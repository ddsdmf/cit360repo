/**
 * This class uses the implements Runnable with the run method
 *
 */
public class UpdateRunnable implements Runnable{
    private Counter counter;

    public UpdateRunnable(Counter counter){
        this.counter = counter;
    }

    public void run(){
        try{
            Thread.sleep(100);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
        counter.increment();
        System.out.println("Counting " + counter.get());
    }
}

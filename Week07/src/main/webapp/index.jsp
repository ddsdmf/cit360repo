<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>JSP - redirect to html page</title>
    <meta http-equiv="refresh" content="2; url = Week07WebPage.html ">
</head>
<body>
<h1><%= "redirect to html page" %>
</h1>
<br/>
<a href="Week07WebPage.html">This is the post example Add Music</a>
</body>
</html>
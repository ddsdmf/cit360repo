import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "Week07Servlet", value = "/Week07Servlet")
public class Week07Servlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");

        // Add new music
        try {
            PrintWriter out = response.getWriter();
            response.setContentType("text/html");
            out.println("<html><head></head><body bgcolor=\"#D9E2F3\">");
            String title = request.getParameter("Title");
            String artist = request.getParameter("Artist");
            String media = request.getParameter("media");

            out.println("<div>");

            out.println("<p class=MsoNormal align=center style='text-align:center;line-height:normal'><b><span\n" +
                    "style='font-size:24.0pt;font-family:\"Times New Roman\",serif;color:black'>New Music Information</span></b></p>");

            out.println("<div align=center>");

            out.println("<table class=MsoTable15Plain3 border=0 cellspacing=0 cellpadding=0\n" +
                    " style='border-collapse:collapse'>");
            out.println("<tr>");
            out.println("<td width=312 valign=top style='width:233.75pt;border:none;border-bottom:\n" +
                    "  solid #7F7F7F 1.0pt;padding:0in 5.4pt 0in 5.4pt'>");
            out.println("<p class=MsoNormal style='line-height:normal'><b><span style='font-size:13.5pt;" +
                    "  font-family:\"Times New Roman\",serif;color:black;text-transform:uppercase'>Music" +
                    "  Title:</span></b></p>\n" +
                    "  </td>");
            out.println("<td width=312 valign=top style='width:233.75pt;border:none;border-bottom:\n" +
                    "  solid #7F7F7F 1.0pt;padding:0in 5.4pt 0in 5.4pt'>");
            out.println("<p class=MsoNormal style='line-height:normal'><b><span style='font-size:13.5pt;" +
                    "  font-family:\"Times New Roman\",serif;color:black;text-transform:uppercase'>" + title + " </span></b></p>\n" +
                    "  </td>\n" +
                    " </tr>");
            out.println("<tr>");
            out.println("<td width=312 valign=top style='width:233.75pt;border:none;border-right:solid #7F7F7F 1.0pt;\n" +
                    "  padding:0in 5.4pt 0in 5.4pt'>");
            out.println("<p class=MsoNormal style='line-height:normal'><b><span style='font-size:13.5pt;\n" +
                    "  font-family:\"Times New Roman\",serif;color:black;text-transform:uppercase'>Artist\n" +
                    "  Name:</span></b></p>\n" +
                    "  </td>");
            out.println("<td width=312 valign=top style='width:233.75pt;padding:\n" +
                    "  0in 5.4pt 0in 5.4pt'>");
            out.println("<p class=MsoNormal style='line-height:normal'><span style='font-size:13.5pt;\n" +
                    "  font-family:\"Times New Roman\",serif;color:black'>" + artist + "</span></p>\n" +
                    "  </td>\n" +
                    " </tr>");
            out.println("<tr>");
            out.println("<td width=312 valign=top style='width:233.75pt;border:none;border-right:solid #7F7F7F 1.0pt;\n" +
                    "  padding:0in 5.4pt 0in 5.4pt'>");
            out.println("<p class=MsoNormal style='line-height:normal'><b><span style='font-size:13.5pt;\n" +
                    "  font-family:\"Times New Roman\",serif;color:black;text-transform:uppercase'>Media\n" +
                    "  Type:</span></b></p>\n" +
                    "  </td>");
            out.println("<td width=312 valign=top style='width:233.75pt;padding:0in 5.4pt 0in 5.4pt'>");
            out.println("<p class=MsoNormal style='line-height:normal'><span style='font-size:13.5pt;\n" +
                    "  font-family:\"Times New Roman\",serif;color:black'>" + media + "</span></p>\n" +
                    "  </td>\n" +
                    " </tr>\n" +
                    "</table>");
            out.println("</div>");

            out.println("<p class=MsoNormal align=center style='text-align:center;line-height:normal'><b><span\n" +
                    "style='font-size:18.0pt;font-family:\"Times New Roman\",serif;color:black'>*** Congratulations\n" +
                    "***</span></b></p>");
            out.println("<p class=MsoNormal align=center style='text-align:center;line-height:normal'><b><span\n" +
                    "style='font-size:16.0pt;font-family:\"Times New Roman\",serif;color:black'>Your\n" +
                    "new music has been added to the library.</span></b></p>");
            out.println("<p class=MsoNormal>&nbsp;</p>");
            out.println("</div>");


            out.println("</body></html>");
        }catch (Exception e){
            System.out.println("HTML code caused an error");
        }
    }
}

/** Author: Drayton Fults
 *  Course: CIT360-03
 *  Date  : 01/27/2021
 *  Module: W04 Assignment: JSON and HTTP or URL
 *  This is the Client side of the assignment
 */
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.net.*;
import java.io.*;
import java.sql.Connection;
import java.util.*;

/**
 * The main method to start the program
 */
public class W04json {
    public static void main(String[] args) {

        try {
            String jsonString = getHTTPPage("http://localhost:8500");
            Persons person2 = jsonToPerson(jsonString);
            System.out.println(person2);
        } catch (Exception e){
            System.out.println("Error Message = " + e.getMessage());
        }

    }//end of main

    /**
     * This get the HTTP page from the server
     * @param urlString URL string
     * @return the content of the HTTP page loaded
     * @throws Exception
     */
    public static String getHTTPPage(String urlString) throws Exception{

        String page="";

        try {

            //convert string to url object
            URL url = new URL(urlString);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();


            BufferedReader bufferReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            // Print the response code
            // and response message from server.
            System.out.println("Response Code:" + connection.getResponseCode());
            System.out.println("Response Message:" + connection.getResponseMessage());
            StringBuilder stringBuilder = new StringBuilder();

            String line = null;
            while ((line= bufferReader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
            page = stringBuilder.toString();

        } catch (Exception e) {
            System.out.println("Error = " + e.getMessage());
            //This stops the program if the HTTP server connection is refused
            throw new Exception(e.getMessage());

        }

        return page;
    }//end of getHTTPPage

    /**
     * this method converts the JSON to an object
     * @param jsonString JSON string
     * @return person object
     */
    public static Persons jsonToPerson(String jsonString){

        //This is creating the ObjectMapper need to convert a string to JSON
        ObjectMapper mapper = new ObjectMapper();
        //Set the person object to null
        Persons person1 = null;

        try {
            person1 = mapper.readValue(jsonString, Persons.class);
        } catch (JsonProcessingException e) {
            System.out.println("Error = " + e.getMessage());
        }

        return person1;

    }// end jsonToPerson

}//end of class

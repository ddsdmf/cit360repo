/** Author: Drayton Fults
 *  Course: CIT360-03
 *  Date  : 01/27/2021
 *  Module: W04 Assignment: JSON and HTTP or URL
 *  This is the HTTP server file for Week 04
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.*;
import com.sun.net.httpserver.HttpServer;
import java.io.*;
import java.net.*;

public class MyHTTPServer {

    /**
     * This is the main method to start the HTTP server
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {

        // Use this to set the port of the HTTP server
        Integer port = 8500;

        //This is use to seet the port number for the HTTP server
        HttpServer myServer = HttpServer.create(new InetSocketAddress(port), 0);
        //This sets the default path for the HTTP server
        HttpContext context = myServer.createContext("/");
        //This call the handler method needed for the HTTP server
        context.setHandler(MyHTTPServer::requestHandler);
        //This starts the server
        System.out.println("=== HTTP Server Running ===");
        System.out.println("=== Server Running on port " + port + " ===");
        System.out.println("Do not forget to end the program when done");
        myServer.start();
    }// end of main

    /**
     * This is the HTTP handler for the HTTP server
     * @param exchange
     * @throws IOException
     */
    private static void requestHandler(HttpExchange exchange) throws IOException {


        //this is the record for the JSON page
        Persons person1 = new Persons();
        person1.setName("Drayton");
        person1.setSurname("Fults");
        person1.setAge(25);
        person1.setCity("Friendswood");

        //This call the method to convert the object to JSON
        String response = personsToJson(person1);

        //response code and length need for the browser to load the page
        exchange.sendResponseHeaders(200, response.getBytes().length);
        //Create the outputstream for the client
        OutputStream outputStream = exchange.getResponseBody();
        //Sends the bytes to client
        outputStream.write(response.getBytes());
        //Closes the outputstream to the client
        outputStream.close();

    }// end of requestHandler

    /**
     * This is used to convert the object to JSON
     * @param person Object sent to method
     * @return JSON string returned
     */
    public static String personsToJson(Persons person){

        //This is creating the ObjectMapper need to convert a string to JSON
        ObjectMapper mapper = new ObjectMapper();
        //setting the jsonString to blank
        String jsonString = "";

        try {
            // This is used to convert the object to a JSON string
            jsonString = mapper.writeValueAsString(person);
        } catch (JsonProcessingException e) {
            //This will display an Error message to the user if there is an error
            System.out.println("Error " + e.getMessage());
        }

        //returns the JSON string
        return jsonString;

    }// end of personsToJson

}

import java.util.List;
import java.util.Scanner;

public class MusicLibrary {
    private static final Scanner scanner;

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_GREEN = "\u001B[32m";


    /** This is a static initializer. It's like a constructor for the class itself.
     * It gets called when the class is first loaded into memory.
     * We'll initialize our scanner in here.
     * This code is from office hours from Brother Gardner
     */
    static {
        //creates a scanner to get user input
        scanner = new Scanner(System.in);
    }//end static

    public static void main(String[] args) {

        String sqlSelect = "from Music";
        MusicDatabase database = MusicDatabase.getInstance();

        //This will return all the records in the database
        List<Music> musicLibrary = database.getMusic(sqlSelect);
        //For each loop to print all records to the console
        for (Music i : musicLibrary){
            System.out.println(i);
        }// end of for each loop

        boolean addMusic = getBoolean("Would you like to add music to the library (y or n):  ");
        if (addMusic) {
            String songTitle = getString("Enter the song title:  ");
            String artist = getString("Enter the artist name:  ");
            String media = getString("Enter the media type (e.g. CD, MP3, DVD...):  ");
            //This adds the music to the database
            database.insertMusic(songTitle,artist,media);
            //clearing the list from the last query
            musicLibrary.clear();
            //pulling a list of all the record is the database after new record added
            musicLibrary = database.getMusic(sqlSelect);

            //for each loop to show all the records from the database
            for (Music i : musicLibrary){
                System.out.println(i);
            }// end of for each loop
        }//end of if statement




    }//end of main

    /**
     * The getValue Method
     *  Purpose: Prompt the user and get a string value
     *  @param: the user prompt as a String
     *  @return: the string value entered by the user
     */
    public static String getString(String prompt)
    {

        // declare a string to store user input
        String value;

        // prompt the user
        System.out.print(prompt);

        // get the input and return it
        value = scanner.nextLine();
        return value;
    }// end of getString

    /**
     * The getValue Method
     *  Purpose: Prompt the user and get a string value
     *  @param: the user prompt as a String
     *  @return: the string value entered by the user
     */
    public static boolean getBoolean(String prompt)
    {

        // declare a boolean to store true or false
        boolean value = true;

        for(int i = 0; i< 1; i++){
            System.out.print(prompt);

            //check if input from user
            if(scanner.hasNext()){
                String input = scanner.nextLine();
                //Check if enter y
                if (input.equalsIgnoreCase("y")){
                    //Stores value
                    value = true  ;
                    //Check if enter n
                }else if (input.equalsIgnoreCase("n")) {
                    //Stores value
                    value = false;
                }else {
                    //display error message if not a y or n
                    System.out.println("Invalid input.");
                    //repeat the loop
                    i -= 1;
                }
            }
        }
        return value;
    }// end of getBoolean
}//end of class

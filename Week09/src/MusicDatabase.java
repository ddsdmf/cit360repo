import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

public class MusicDatabase {

    SessionFactory factory = null;
    Session session = null;

    private static MusicDatabase single_instance = null;

    private MusicDatabase() {
        factory = HibernateUtils.getSessionFactory();
    }//end of method

    /**
     * setup the singleton to get the instance of the class
     * @return
     */
    public static MusicDatabase getInstance(){
        if (single_instance == null){
            //This is run if single_instance is null
            single_instance = new MusicDatabase();
        }//end of if statement
        return single_instance;
    }//end of getInstance

    //this returns all records in the database


    /**
     *  this returns all records in the database
     * @param sqlSelect SQL Select statement to run
     * @returnrecords from database
     */
    public List<Music> getMusic(String sqlSelect){
        //Use this to return all records from the database
        try {
            session = factory.openSession();
            session.getTransaction().begin();
            //String sqlSelect = "from Music where id=100";
            List<Music> music = (List<Music>)session.createQuery(sqlSelect).getResultList();
            session.getTransaction().commit();
            return music;
        } catch (Exception e) {
            System.out.println("Error rollback running");
            session.getTransaction().rollback();
            return null;
        }finally {
            //This closes the session if it works or not
            session.close();
        }//end of try catch finally
    }//end of getMusic method

    public void insertMusic(String songTitle, String artist, String media){
        //Use this to add new music to the library
        try {
            session = factory.openSession();
            session.beginTransaction();
            Music newMusic = new Music();
            newMusic.setSongTitle(songTitle);
            newMusic.setArtist(artist);
            newMusic.setMedia(media);
            session.save(newMusic);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.out.println("New music was not added to database");
            session.getTransaction().rollback();
        }finally {
            session.close();
        }//end of try catch finally
    }//end of insertMusic

}

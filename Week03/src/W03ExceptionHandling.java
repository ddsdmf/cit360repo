/** Author: Drayton Fults
 *  Course: CIT360-03
 *  Date  : 01/14/2021
 *  Module: W02 Assignment: W02 Java Collections
 */

import java.util.*;
import java.util.Scanner;



public class W03ExceptionHandling {

    private static final Scanner scanner;

    /** This is a static initializer. It's like a constructor for the class itself.
     * It gets called when the class is first loaded into memory.
     * We'll initialize our scanner in here.
     * This code is from office hours from Brother Gardner
     */
    static {
        //creates a scanner to get user input
        scanner = new Scanner(System.in);
    }//end static


    public static void main(String[] args) {

        int num1   = 0;
        int num2   = 0;
        int answer = 0;

        //The following lines get two numbers from the user
        num1 = getInt("Enter number 1:  ");
        num2 = getInt("Enter number 2:  ");

        //The call the divide method to divide two numbers and display the output

        do {
            try {
                answer = divide(num1, num2);
                break;
            }
            catch (ArithmeticException e) {
                //messages displayed when ArithmeticException is thrown
                System.out.println("=== Cannot divide by 0 ===");
                System.out.println("Enter a non 0 number");
            }
            finally {
                //This message is displayed each time the try is run with or without an exception being thrown
                System.out.println("This is the finally message that will be display all the time");
                System.out.println("This message will display everytime the try is run");
            }
            //This prompts the user to enter number 2 again it 0 was entered for number 2
            num2 = getInt("Enter number 2:  ");
        } while (true);

        //This displays the answer for the two number entered
        System.out.println("Answer for " + num1 + "/" + num2 + " = " + answer);

    }// end of main

    /**
     * this method divides two number passed into the method
     * @param num1 integer passed in
     * @param num2 integer passed in
     * @return answer for num1 divided by num2
     * @throws ArithmeticException
     */
    public static Integer divide(int num1, int num2) throws ArithmeticException{

        int answer = 0;

        //divides two numbers
        answer = num1/num2;

        //value returned
        return answer;
    }// end of divide


    /**
     * This method asked the user for an integer. If an integer is not entered
     * it asks the user to try again to enter an integer
     * @param prompt String that is the prompt to the user
     * @return integer entered by the user
     */
    public static Integer getInt(String prompt){

        int number = 0;

        for (int count = 0; count < 1; count++) {
            //prompt the user
            System.out.print(prompt);
            // if statement to make sure value entered is an int
            if (scanner.hasNextInt()) {
                // stores the number if it is an int
                number = scanner.nextInt();
            } else {
                //display error message if not a double
                System.out.println("Invalid input. Enter numbers only.");
                scanner.next();
                count -= 1;
            }//end of if else statement
        }// end of for loop
        //value returned
        return number;

    }// end of getInt

}// end of class

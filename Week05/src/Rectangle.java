/** Author: Drayton Fults
 *  Course: CIT360-03
 *  Date  : 02/4/2021
 *  Module: W05 Assignment: JUnit
 *  This is the class that the JUnits test will test
 */

import java.security.PrivateKey;
import java.util.Map;
import java.util.Random;

/**
 * This is the Rectangle class for
 * finding the area and perimeter of a rectangle
 */
public class Rectangle {
    private double width;
    private double height;
    private String color;
    private Boolean filled;

    public Rectangle() {
    }

    /** Constructor with just width and height parameters */
    public Rectangle(double width, double height) {
        this.width = width;
        this.height = height;
    }

    /** Constructor with all four parameters */
    public Rectangle(
            double width, double height, String color, boolean filled) {
        this.width = width;
        this.height = height;
        this.color = color;
        this.filled = filled;
    }

    /** Returns the color */
    public String getColor() {
        return color;
    }

    /** set the color */
    public void setColor(String color) {
        this.color = color;
    }

    /** Returns the filled status */
    public Boolean getFilled() {
        return filled;
    }

    /** Set filled status */
    public void setFilled(Boolean filled) {
        this.filled = filled;
    }

    /** Return width */
    public double getWidth() {
        return width;
    }

    /** Set a new width */
    public void setWidth(double width) {
        this.width = width;
    }

    /** Return height */
    public double getHeight() {
        return height;
    }

    /** Set a new height */
    public void setHeight(double height) {
        this.height = height;
    }

    /** Return area */
    public double getArea() {
        return width * height;
    }

    /** Return perimeter */
    public double getPerimeter() {
        return 2 * (width + height);
    }

    public double getRandomArea(){
        double area = 0;

       double randomWidth = Math.random();
       double randomHeight = Math.random();
       double guess = Math.random();
       area = randomHeight * randomWidth;
       try{
           //This could be a very long loop
           while (guess > area){
               guess = Math.random();
           }
       }catch (Exception e){
           System.out.println("Error guessing random area");
       }
        return guess;
    }
}

import org.junit.jupiter.api.*;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.*;

class RectangleTest {
    Rectangle myRectangle;

    @BeforeEach
    @DisplayName("Run before each test method")
    void runBeforeEach(){
        myRectangle = new Rectangle();
        myRectangle.setColor("Blue");
        myRectangle.setHeight(5);
        myRectangle.setWidth(5);
        myRectangle.setFilled(true);
    }

    @Test
    @DisplayName("Get the color of the rectangle")
    void getColor() {
        String color = "Blue";
        assertEquals(color,myRectangle.getColor(),"This should get the color and it should " + color);
    }

    @Test
    @DisplayName("Set the color to green")
    void setColor() {
        String color = "Green";
        myRectangle.setColor(color);
        assertEquals(color,myRectangle.getColor(),"This should get the color and it should " + color);
    }

    @Test
    @DisplayName("Is the rectangle filled")
    void getFilled() {
        assertTrue(myRectangle.getFilled(),"This should be true");
    }

    @Test
    @DisplayName("Set filled to false")
    void setFilled() {
        myRectangle.setFilled(false);
        assertFalse(myRectangle.getFilled());
    }

    @Test
    @DisplayName("Get the width of the rectangle")
    void getWidth() {
        assertEquals(5,myRectangle.getWidth());
    }

    @Test
    @DisplayName("Set the width and test it")
    void setWidth() {
        double width = 10.2;
        myRectangle.setWidth(width);
        assertEquals(width,myRectangle.getWidth());
    }

    @Test
    @DisplayName("Get the height of the rectangle")
    void getHeight() {
        assertEquals(5,myRectangle.getHeight());
    }

    @Test
    @DisplayName("Set the height and test it")
    void setHeight() {
        double height = 10.2;
        myRectangle.setHeight(height);
        assertEquals(height,myRectangle.getHeight());
    }

    @Test
    void getArea() {
        double expectedArea = 25;
        assertEquals(expectedArea,myRectangle.getArea());
    }

    @Test
    void getPerimeter() {
        double expectedPerimeter = 20;
        assertEquals(expectedPerimeter,myRectangle.getPerimeter());
    }

    @Test
    @DisplayName("Test all the parms at one test")
    void testAll(){
        assertAll("This is testing all the parms",
                () -> assertNotNull(myRectangle.getColor()),
                () -> assertNotNull(myRectangle.getFilled()),
                () -> assertEquals(5, myRectangle.getHeight()),
                () -> assertEquals(5, myRectangle.getWidth()),
                () -> assertEquals(25,myRectangle.getArea()),
                () -> assertEquals(20,myRectangle.getPerimeter()),
                () -> assertTrue(myRectangle.getFilled()));
    }

    @Test
    @DisplayName("Test the constructor with two parameters")
    void twoParmRectangle(){
        Rectangle myRectangle2 = new Rectangle(6,6);
        assertAll("Make sure the two parm rectangle worked",
                () -> assertEquals(36,myRectangle2.getArea()),
                () -> assertEquals(24,myRectangle2.getPerimeter()),
                () -> assertNull(myRectangle2.getColor()),
                () -> assertNull(myRectangle2.getFilled()));
    }

    @Test
    @DisplayName("Create an object and see if it is the same")
    void checkObjectsSame(){
        Rectangle myRectangle2 = new Rectangle(5,5,"Blue",false);
        assertAll("Checking two objects",
                () -> assertNotSame(myRectangle.getFilled(),myRectangle2.getFilled()),
                () -> assertSame(myRectangle.getColor(),myRectangle2.getColor()));

    }

    @Test
    @DisplayName("Test arrays")
    void testArrays(){
        //This is not the way an assertArrayEquals would be done
        String[] array1 = {"Rectangle","Circle","Square","Triangle"};
        String[] array2 = {"Rectangle","Circle","Square","Triangle"};
        assertArrayEquals(array1, array2);
    }

    @Test
    @DisplayName("This test will not be tested because of the disable command")
    @Disabled
    void notTested(){
        fail("This will not be tested because of the disable command");
    }

    @Test
    @DisplayName("This stops a very long loop")
    void testLongLoop(){
        assertTimeoutPreemptively(Duration.ofSeconds(10),
                () -> myRectangle.getRandomArea() > 0);
    }
}
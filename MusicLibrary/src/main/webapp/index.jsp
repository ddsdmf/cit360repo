<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Music Library</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.0/font/bootstrap-icons.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>


</head>
<body class="d-flex flex-column min-vh-100">
    <div class="container m-3">
<h1><%= "Welcome to the Music Library" %>
</h1></div>
<br/>
    <div class="container">
    <a href="AddMusic.html" class="btn mb-3 col-3 btn-primary"><i class="bi bi-plus-circle"></i> Add New Music</a>
    <a class="btn mb-3 col-3 btn-primary" href="DisplayMusicServlet">Display Music</a>
    </div>
</body>
</html>
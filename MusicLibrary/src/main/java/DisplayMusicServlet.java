

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(name = "DisplayMusicServlet", value = "/DisplayMusicServlet")
public class DisplayMusicServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        displayMusic(request, response, "GET");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        displayMusic(request, response, "POST");
    }

    protected void displayMusic(HttpServletRequest request, HttpServletResponse response, String requestType) throws ServletException, IOException{
        System.out.println("This is the DisplayMusic Servlet: " + requestType);



        try{
            MusicDatabase database = MusicDatabase.getInstance();
            String sqlSelect = "from Music";
            //This will return all the records in the database
            List<Music> musicLibrary = database.getMusic(sqlSelect);

            //Start of the HTML page
            PrintWriter out = response.getWriter();
            response.setContentType("text/html");
            out.println("<html><head><title>Music List</title> + " +
                            "    <link rel=\"stylesheet\" href=\"https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.0/font/bootstrap-icons.css\">" +
                            "    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css\">\n" +
                            "    <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js\"></script>\n" +
                            "    </head><body bgcolor=\"#D9E2F3\">");
            out.println("<body class=\"d-flex flex-column min-vh-100\">");
            out.println("<div class=\"container\">");
            out.println("<div class=\"container\">");
            out.println("<h1 class=\"mb-3\">Music Library</h1>");
            out.println("<h3 class=\"mb-3\">Music List</h3>");
            out.println("<table class=\"table table-sm table-bordered table-hover\">");
            out.println("<thead class=\"thead-light\">");
            out.println("<tr>");
            out.println("<th>ID Num</th>");
            out.println("<th>Title</th>");
            out.println("<th>Artist</th>");
            out.println("<th>Media Type</th>");
            out.println("</tr>");
            out.println("</thead>");
            for (Music i : musicLibrary){
                out.println("<tr>");
                out.println("<td>" + i.getId() + "</td>");
                out.println("<td>" + i.getTitle() + "</td>");
                out.println("<td>" + i.getArtist() + "</td>");
                out.println("<td>" + i.getMediaType() + "</td>");
                out.println("</tr>");
            }// end of for each loop
            out.println("</table>");
            out.println("</div>");
            out.println("<a href=\"AddMusic.html\" class=\"btn mb-3 col-3 btn-primary\"><i class=\"bi bi-plus-circle\"></i> Add New Music</a>");
            out.println("<a href=\"index.jsp\" class=\"btn mb-3 col-3 btn-primary\">Menu</a>");
            out.println("</div>");
            out.println("</body></html>");
        } catch(IOException e) {
            System.out.println("error building page");
        }
    }
}

/** Author: Drayton Fults
 *  Course: CIT360-03
 *  Date  : 01/14/2021
 *  Module: W02 Assignment: W02 Java Collections
 */

import java.util.*;
import java.util.Scanner;



public class W02JavaCollections {

    private static final Scanner scanner;

    /** This is a static initializer. It's like a constructor for the class itself.
     * It gets called when the class is first loaded into memory.
     * We'll initialize our scanner in here.
     * This code is from office hours from Brother Gardner
     */
    static {
        //creates a scanner to get user input
        scanner = new Scanner(System.in);
    }//end static

    /**
     * main method for the java program
     * @param args
     */
    public static void main(String[] args) {

        Double number1;
        Double number2;
        Double number3;
        Double number4;
        Double number5;
        String string1;
        String string2;
        String string3;
        String string4;
        String string5;





        //Ask the user to enter 5 numbers as doubles
        List<Double> myDoubles = getDoubles("Enter 5 numbers: ", 5);

        //setting the doubles so the user will only have to do this one time to play with then different collections
        number1 = myDoubles.get(0);
        number2 = myDoubles.get(1);
        number3 = myDoubles.get(2);
        number4 = myDoubles.get(3);
        number5 = myDoubles.get(4);

        //Ask the user to enter 5 strings
        List<String> myStrings = getStrings("Enter 5 Strings: ", 5);

        //setting the strings so the user will only have to do this one time to play with different collections
        string1 = myStrings.get(0);
        string2 = myStrings.get(1);
        string3 = myStrings.get(2);
        string4 = myStrings.get(3);
        string5 = myStrings.get(4);


        //call displayDoubleList to show Java List Collection on Doubles
        displayDoubleList(number1,number2,number3,number4,number5);

        //call displayStringList to show Java List Collection on Strings
        displayStringList(string1,string2,string3,string4,string5);

        //call displayDoubleQueue to show Java Queue Collection on Doubles
        displayDoubleQueue(number1,number2,number3,number4,number5);

        //call displayStringQueue to show Java Queue Collection on Strings
        displayStringQueue(string1,string2,string3,string4,string5);

        //call displaySetDoubles to show Java Set Collection on Doubles
        displaySetDoubles(number1,number2,number3,number4,number5);

        //call displaySetStrings to show Java Set Collection on Strings
        displaySetStrings(string1,string2,string3,string4,string5);

        //call displayMap to show Java Map collection using the strings and Doubles
        displayMap(string1,string2,string3,string4,string5,number1,number2,number3,number4,number5);





    }//end of main

    /**
     * getDoubles method is used to get a list of numbers
     * @param prompt the message users see
     * @param numberofDoubles this is the number of doubles to be entered my user
     * @return sends ArrayList back
     */
    public static ArrayList<Double> getDoubles(String prompt, int numberofDoubles){
        ArrayList<Double> myDoubles = new ArrayList<Double>();

        //display prompt
        System.out.println(prompt);
        do{
            //check to see if double value entered
            if (scanner.hasNextDouble()){
                //add the double value to array
                myDoubles.add(scanner.nextDouble());
            } else {
                // if non-double value display message
                System.out.println("Invalid input");
                scanner.next();
            }//end if statement
        } while ( myDoubles.size() < numberofDoubles);//end do loop
        return myDoubles;
    }// end of getDoubles

    /**
     * getStrings method is used to get a list of numbers
     * @param prompt the message users see
     * @param numberofStrings this is the number of strings to be entered my user
     * @return sends ArrayList back
     */
    public static ArrayList<String> getStrings(String prompt, int numberofStrings){
        ArrayList<String> myStrings = new ArrayList<String>();

        //display prompt
        System.out.println(prompt);
        do{
            //check to see if string entered
            if (scanner.hasNext()){
                //add the string value to array
                myStrings.add(scanner.next());
            } else {
                // if non-string value display message
                System.out.println("Invalid input");
                scanner.next();
            }//end if statement
        } while ( myStrings.size() < numberofStrings);//end do loop
        return myStrings;
    }// end of getStrings


    /**
     * This is used to show different things that can be done with List that are String
     * passing in the 5 strings entered by the user then adding them to ArrayList using
     * add, sort, and remove
     * @param string1
     * @param string2
     * @param string3
     * @param string4
     * @param string5
     */
    public static void displayStringList(String string1, String string2, String string3, String string4, String string5){

        //creating ArrayList from string passed to method
        ArrayList<String> myStrings = new ArrayList<>();
        //adding values to the Arraylist
        myStrings.add(string1);
        myStrings.add(string2);
        myStrings.add(string3);
        myStrings.add(string4);
        myStrings.add(string5);

        //Message to user
        printLine();
        System.out.println("This is the List Collection using strings");
        printLine();

        //display the strings entered by the users in the same ordered entered
        System.out.println("--- This is your list of Strings not sorted ---");
        myStrings.forEach(System.out::println);
        printLine();

        //display the strings entered by the users in the same ordered entered
        System.out.println("--- This is your list of Strings sorted ---");
        myStrings.sort(String::compareToIgnoreCase);
        myStrings.forEach(System.out::println);
        printLine();

        //remove item 2 from the list and display to new list values
        System.out.println("--- Removing item 2 from the list and displaying the values in the list ---");
        System.out.println("Removing:  " + myStrings.get(2));
        myStrings.remove(2);
        System.out.println("---List with item removed ---");
        myStrings.forEach(System.out::println);
        printLine();

    }//end displayStringList

    /**
     * This is used to show different things that can be done with List that are Doubles
     *      passing in the 5 doubles entered by the user then adding them to ArrayList using
     *      add, and sort
     * @param double1
     * @param double2
     * @param double3
     * @param double4
     * @param double5
     */
    public static void displayDoubleList(Double double1, Double double2, Double double3, Double double4, Double double5){

        //creating ArrayList from doubles passed to method
        ArrayList<Double> myDoubles = new ArrayList<>();
        //adding values to the Arraylist
        myDoubles.add(double1);
        myDoubles.add(double2);
        myDoubles.add(double3);
        myDoubles.add(double4);
        myDoubles.add(double5);

        //Message to user
        printLine();
        System.out.println("This is the List Collection using doubles");
        printLine();

        //display the numbers entered by the users in the same ordered entered
        System.out.println("--- This is your list of Doubles not sorted ---");
        myDoubles.forEach(System.out::println);
        printLine();

        //display the numbers entered by the users sorted
        System.out.println("--- This is your list of Doubles sorted ---");
        myDoubles.sort(null);
        myDoubles.forEach(System.out::println);
        printLine();

    }//end of displayDoubleList

    /**
     * This is used to show different things that can be done with Queue Java Collections
     * using some of the methods like poll, peek, add and size
     * @param double1
     * @param double2
     * @param double3
     * @param double4
     * @param double5
     */
    public static void displayDoubleQueue(Double double1, Double double2, Double double3, Double double4, Double double5){

        //creating Queue LinkedList from doubles passed to method
        Queue<Double> myDoubles = new LinkedList<>();
        //adding values to the Queue
        myDoubles.add(double1);
        myDoubles.add(double2);
        myDoubles.add(double3);
        myDoubles.add(double4);
        myDoubles.add(double5);

        //Message to user
        printLine();
        System.out.println("This is the Queue Collection using doubles");
        printLine();

        //display the numbers entered by the users in the same ordered entered
        System.out.println("--- This is the item of Doubles Queue not sorted ---");
        myDoubles.forEach(System.out::println);
        System.out.println("The current number of items in the Queue:  " + myDoubles.size());
        printLine();

        //This is using he peek method of the Queue to display the first item in the Queue without removing it
        System.out.println("--- This is using the peek method of a Queue to display the first item in the queue---");
        System.out.println("First item:  " + myDoubles.peek());
        System.out.println("Current values in the Queue LinkedList");
        myDoubles.forEach(System.out::println);
        System.out.println("The current number of items in the Queue:  " + myDoubles.size());
        printLine();

        //This is using he poll method of the Queue to remove and display the first item in the Queue
        System.out.println("--- This is using the poll method of a Queue to remove the first item in the queue and display it---");
        System.out.println("Removing:  " + myDoubles.poll());
        System.out.println("Current values in the Queue LinkedList");
        myDoubles.forEach(System.out::println);
        System.out.println("The current number of items in the Queue:  " + myDoubles.size());
        printLine();


    }//end of displayDoubleQueue


    /**
     * This is used to show different things that can be done with Queue Java Collections
     * using some of the methods like poll, peek, add and size with string data
     * @param string1
     * @param string2
     * @param string3
     * @param string4
     * @param string5
     */
    public static void displayStringQueue(String string1, String string2, String string3, String string4, String string5){

        //creating Queue LinkedList from strings passed to method
        Queue<String> myStrings = new LinkedList<>();
        //adding values to the Queue
        myStrings.add(string1);
        myStrings.add(string2);
        myStrings.add(string3);
        myStrings.add(string4);
        myStrings.add(string5);

        //Message to user
        printLine();
        System.out.println("This is the Queue Collection using strings");
        printLine();

        //display the numbers entered by the users in the same ordered entered
        System.out.println("--- This is the item of Doubles Queue not sorted ---");
        myStrings.forEach(System.out::println);
        System.out.println("The current number of items in the Queue:  " + myStrings.size());
        printLine();

        //This is using he peek method of the Queue to display the first item in the Queue without removing it
        System.out.println("--- This is using the peek method of a Queue to display the first item in the queue---");
        System.out.println("First item:  " + myStrings.peek());
        System.out.println("Current values in the Queue LinkedList");
        myStrings.forEach(System.out::println);
        System.out.println("The current number of items in the Queue:  " + myStrings.size());
        printLine();

        //This is using he poll method of the Queue to remove and display the first item in the Queue
        System.out.println("--- This is using the poll method of a Queue to remove the first item in the queue and display it---");
        System.out.println("Removing:  " + myStrings.poll());
        System.out.println("Current values in the Queue LinkedList");
        myStrings.forEach(System.out::println);
        System.out.println("The current number of items in the Queue:  " + myStrings.size());
        printLine();


    }//end of displayStringQueue

    /**
     * This is using the Java Set Collection to show some of the uses. The main use in this method was
     * to show duplicate and non-duplicate values entered by the user
     * @param double1
     * @param double2
     * @param double3
     * @param double4
     * @param double5
     */
    public static void displaySetDoubles(Double double1, Double double2, Double double3, Double double4, Double double5){

        //creating Set HashSet from doubles passed to method
        Set<Double> myDoubles = new HashSet<>();
        //adding values to Set
        myDoubles.add(double1);
        myDoubles.add(double2);
        myDoubles.add(double3);
        myDoubles.add(double4);
        myDoubles.add(double5);

        //Message to user
        printLine();
        System.out.println("This is the Set Collection using doubles");
        printLine();

        //display the numbers entered by the users
        System.out.println("--- This is the item of Doubles Set ---");
        System.out.println("If there were duplicates only one of the values will be displayed");
        myDoubles.forEach(System.out::println);
        System.out.println("The current number of items in the Set:  " + myDoubles.size());
        printLine();

        //this is to find the duplicate values entered by the user if any
        Set<Double> nonDuplicates = new HashSet<>();
        Set<Double> duplicates = new HashSet<>();
        if (!nonDuplicates.add(double1)){
            duplicates.add(double1);
        }
        if (!nonDuplicates.add(double2)){
            duplicates.add(double2);
        }
        if (!nonDuplicates.add(double3)){
            duplicates.add(double3);
        }
        if (!nonDuplicates.add(double4)){
            duplicates.add(double4);
        }
        if (!nonDuplicates.add(double5)){
            duplicates.add(double5);
        }

        //This removes the duplicate values from the non-duplicate list
        nonDuplicates.removeAll(duplicates);

        //This prints only the values that have no duplicates
        printLine();
        System.out.println("Non-duplicate values: ");
        if (nonDuplicates.size() > 0) {
            nonDuplicates.forEach(System.out::println);
        }else {
            System.out.println("All values were duplicated");
        }

        //This prints the values that were duplicated when entered by the user
        printLine();
        System.out.println("Values that are duplicated: ");
        if (duplicates.size() > 0) {
            duplicates.forEach(System.out::println);
        }else {
            System.out.println("No duplicates were entered");
        }
        printLine();

    }// end of displaySetDoubles

    /**
     * This is using the Java Set Collection to show some of the uses. The main use in this method was
     * to show duplicate and non-duplicate values entered by the user
     * @param string1
     * @param string2
     * @param string3
     * @param string4
     * @param string5
     */
    public static void displaySetStrings(String string1, String string2, String string3, String string4, String string5){

        //creating Set HashSet from strings passed to method
        Set<String> myStrings = new HashSet<>();
        //adding values to Set
        myStrings.add(string1);
        myStrings.add(string2);
        myStrings.add(string3);
        myStrings.add(string4);
        myStrings.add(string5);

        //Message to user
        printLine();
        System.out.println("This is the Set Collection using strings");
        printLine();

        //display the strings entered by the users
        System.out.println("--- This is the item of String Set ---");
        System.out.println("If there were duplicates only one of the values will be displayed");
        myStrings.forEach(System.out::println);
        System.out.println("The current number of items in the Set:  " + myStrings.size());
        printLine();

        //this is to find the duplicate values entered by the user if any
        Set<String> nonDuplicates = new HashSet<>();
        Set<String> duplicates = new HashSet<>();
        if (!nonDuplicates.add(string1)){
            duplicates.add(string1);
        }
        if (!nonDuplicates.add(string2)){
            duplicates.add(string2);
        }
        if (!nonDuplicates.add(string3)){
            duplicates.add(string3);
        }
        if (!nonDuplicates.add(string4)){
            duplicates.add(string4);
        }
        if (!nonDuplicates.add(string5)){
            duplicates.add(string5);
        }

        //This removes the duplicate values from the non-duplicate list
        nonDuplicates.removeAll(duplicates);

        //This prints only the values that have no duplicates
        printLine();
        System.out.println("Non-duplicate values: ");
        if (nonDuplicates.size() > 0) {
            nonDuplicates.forEach(System.out::println);
        }else {
            System.out.println("All values were duplicated");
        }

        //This prints the values that were duplicated when entered by the user
        printLine();
        System.out.println("Values that are duplicated: ");
        if (duplicates.size() > 0) {
            duplicates.forEach(System.out::println);
        }else {
            System.out.println("No duplicates were entered");
        }
        printLine();

    }// end of displaySetStrings

    public static void displayMap(String string1, String string2, String string3, String string4, String string5,Double double1, Double double2, Double double3, Double double4, Double double5){

        //Create the Map collection
        Map<Double, String > myMap = new TreeMap<>();
        // adding the key and values to the map
        myMap.put(double1,string1);
        myMap.put(double2,string2);
        myMap.put(double3,string3);
        myMap.put(double4,string4);
        myMap.put(double5,string5);

        //Message to user
        printLine();
        System.out.println("This is the Map Collection examples");
        printLine();

        //prints the key and value from the map
        System.out.println("This is the output showing the key and value");
        System.out.println(myMap);
        printLine();

        //This is how to change and value in the map
        //We will change the second value in the map to the value of CHANGED
        System.out.println("This is changing the value of the second key to CHANGED");
        System.out.println("We are changing " + myMap.get(double2) + " to CHANGED");
        myMap.put(double2,"CHANGED");
        System.out.println(myMap);
        printLine();

        //This is how to remove a value from a map
        //we will remove the key that we changed to CHANGED
        System.out.println("This is to remove the value and key that we changed to CHANGED");
        System.out.println("We are removing " + myMap.get(double2));
        myMap.remove(double2);
        System.out.println(myMap);
        printLine();

        //This is to loop through the items in the myMap collection and print them to the screen
        System.out.println("Here are the keys and value of the myMap object");
        for(Map.Entry myMapItems:myMap.entrySet()){
            Double mapKey = (Double) myMapItems.getKey();
            String mapValue = (String) myMapItems.getValue();
            System.out.println("Key:  " + mapKey + " = " + mapValue);
        }//end of foreach loop




    }//end of displayMap

    /**
     * This is just to print a line when called
     */
    public static void printLine(){
        System.out.println("------------------------------");
    }//end of printline


}//end of W02JavaCollections class
